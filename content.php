<?php
defined('_EXEC') or exit();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div id="breadcrumbs">

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-sm-4" >
            <div class="row">
                <div class="col-sm-12">
                    <hr/>
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputFile" lang="ru">
                            <label class="custom-file-label" for="inputFile"  data-browse="...">Выберите файл</label>
                        </div>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary btn-sm btn-block" id="btnUpload">
                                Upload
                            </button>
                        </div>
                    </div>
                    <div class="ajax-respond"></div>

                    <!--                    <div class="custom-file">-->
                    <!--                        <input type="file" class="custom-file-input" id="customFileLang" lang="ru">-->
                    <!--                        <label class="custom-file-label" for="customFileLang" data-browse="...">-->
                    <!--                            Выберите файл-->
                    <!--                        </label>-->
                    <!--                    </div>-->

                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-toolbar mb-3" role="toolbar">
                        <div class="btn-group-sm mr-3" role="group">
                            <button type="button" class="btn btn-outline-secondary" onclick="addCategory();">+</button>
                            <button type="button" class="btn btn-outline-secondary" onclick="deleteCategory();">-</button>
                            <button type="button" class="btn btn-outline-secondary" onclick="renameCategory();">...</button>
                        </div>
                        <div class="btn-group-sm" role="group">
                            <button type="button" class="btn btn-outline-secondary" onclick="deleteAllCategories();">
                                Очистить
                            </button>
                            <button type="button" class="btn btn-secondary" onclick="help();">?</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div>
                        <form action="get_file.php" method="post" role="form">
                            <div class="form-group">
                                <input type="hidden" class="hidden" name="categories_data" id="categories_data">
    <!--                            <button type="submit" class="btn btn-outline-primary btn-sm btn-block"-->
    <!--                                    onclick="$('#categories_data').val('JSON.stringify(categories_data)');" >-->
    <!--                                Сформировать файл для OpenCart-->
    <!--                            </button>-->
                                <button type="submit" class="btn btn-outline-primary btn-sm btn-block"
                                        onclick="getFile()">
                                    Сформировать файл для OpenCart
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div>
                        <br />
                        <h5>
                            Категории
                        </h5>
                        <hr />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="" >
                        <div id="jstree_div">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col col-sm-8">
            <hr/>
            <div class="" >
                <h5>Товары</h5>
                <? //require_once '../../public_html/views/jumbotron/OpenCart.php';?>
                <div id="products"></div>
            </div>
        </div>
    </div>
</div>
