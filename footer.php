<?php
defined('_EXEC') or exit();
?>
<script src="https://cdn.ingdg.com/get/js/jquery/jquery-3.4.1.min.js"></script>
<script src="static/jstree/jstree.min.js"></script>

<!--<script async src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>-->
<!--<script async src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>-->
<!--<script async src="https://cdn.ingdg.com/get/js/popper.js/popper.min.js"></script>-->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138257986-2"></script>-->
<script async src="https://cdn.ingdg.com/get/css/bootstrav v4.0.0/js/bootstrap.min.js"></script>
<!--<script async src="https://wf.ingdg.com/AuthData"></script>-->

<script>
    // Upload file
    var category_files;

    $('#inputFile').on('change',function(){
        category_files = this.files;
        $(this).next('.custom-file-label').html($(this).val());
    })


    $('#btnUpload').click(function( event ){
        event.stopPropagation();
        event.preventDefault();

        let data = new FormData();
        $.each( category_files, function( key, value ){
            data.append( key, value );
        });

        $.ajax({
            url: './upload.php',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Не обрабатываем файлы (Don't process the files)
            contentType: false, // Так jQuery скажет серверу что это строковой запрос
            success: function( respond, textStatus, jqXHR ){

                // ОК

                if( typeof respond.error === 'undefined' ){

                    category_next_id = respond.category_next_id;
                    category_tree = respond.tree;

                    createTree();

                    let html = '';
                    $.each( respond.files, function( key, val ){ html += val +'<br>'; } )
                    $('.ajax-respond').html( html );
                }
                else{
                    console.log('ОШИБКИ ОТВЕТА сервера: ' + respond.error );
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                console.log('ОШИБКИ AJAX запроса: ' + textStatus);
            }
        });

    });
</script>


<!--<script async src="static/tree.js"></script>-->
<script>

    var categories_data = {},
    category_tree = [],
    category_next_id = 1;

    var node_template = {
        "type": "default",
        "text" : "Новая категория",
        // 'url' : function (node) {
        //     return node.id === '#' ?
        //         'ajax_demo_roots.json' : 'ajax_demo_children.json';
        'url' : function (node) {
            getProducts(node)
            return '';
        },
        // 'data' : function (node) {
        //     return { 'id' : node.id };
        // }
    };

    var tree_config = {
        "core" : {
            "animation" : 1,
            "check_callback" : true,
            "themes" : { "stripes" : false },
            "multiple" : false,
            'data' : null,
        },
        "types" : {
            "#" : {
                // "max_children" : 1,
                "max_depth" : 10,
                "valid_children" : ["root", "default"]
            },
            "root" : {
                // "icon" : "/static/3.3.10/assets/images/tree_icon.png",
                "valid_children" : ["default"]
            },
            "default" : {
                // "valid_children" : ["default"]
            },
            "loading" : {
                "icon" : "./static/Spinner-0.9s-31px.gif",
            },

        },
        "plugins" : [
            "dnd",
            // "unique",
            "search",
            "sort",
            "state", "types", "wholerow",
            // "checkbox",
        ]
    };

    createTree();

    /////////////////////////
    function help() {

        let tree = [];
        let t = $("#jstree_div").jstree(true).get_json('#', {'flat': true});
        let cnt = 0;

        console.log(t);

        return true;

        let ref = $('#jstree_div').jstree(true),
            sel = ref.get_selected();

        if(!sel.length) {
            console.log("n");
            return false;
        }
        console.log(sel);
        console.log(ref);

        console.log(ref.get_text(sel));
        ref.show_dots(sel)
    };


    function getProductsForAllCategories(){
        $('#jstree_div').jstree(true).each_node(function (node) {
            if (this.is_leaf(node) && node.data[attribute]) {
                getProducts(node);
            }
        });
    }

    function getProducts(ref) {
        let path = getCurrentPath(ref),
            time = performance.now();

        if(! path)
            return false;

        $('#products').text("Загрузка...").show();

        if ( ! (path in categories_data)){
            let url = 'https://api.ingdg.com/api/productoffers/method/get/' + encodeURI(getCurrentPath(ref, true)) + '/';
            //url = 'ajax.php';
            ref.instance.set_type(ref.node, 'loading');
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                // async: false,
                success: function (d) {
                            categories_data[path] = d;
                            showProducts(ref, path);
                            ref.instance.set_type(ref.node, 'default');
                            console.log('Запрос:', path, 'Время выполнения запроса (мкс):', performance.now() - time
                            );
                        },
            });
        } else {
            showProducts(ref, path);
        }
    }


    function showProducts(ref, path) {
        let current_path = '',
            products_text = "Выберите категорию";

        if(ref && ref.selected && ref.selected.length) {
            current_path = ref.instance.get_path(ref.node, '/', null);
        }
        console.log("Path:", path, current_path);
        if (path === current_path) {
            products_text = 'В данной категории продукты отсутствуют: ';
            if (path in categories_data) {
                products_text = '';
                // for (let [key, value] of Object.entries(meals)) {
                //     console.log(key + " -> " + p[key])
                // }
                Object.keys(categories_data[path]).forEach(function (item, idx) {
                    products_text += '<div class="form-check"> \
                        <input class="form-check-input" type="checkbox" checked="checked" value="" id="'
                        + categories_data[path][item].model[item].id
                        + '" onclick="selectProductItem('
                        + '\'' + path + '\', '
                        + categories_data[path][item].model[item].id
                        + ')"> \
                        <label class="form-check-label" for="' + categories_data[path][item].model[item].id +'">'
                        + categories_data[path][item].model[item].name +
                        '</label></div>'
                    ;
                });
            }
        }
        $('#products').html(products_text).show();
    }


    function selectProductItem(path, product_id) {

    }


    function getFile() {
        $('#categories_data').val(JSON.stringify(categories_data));
        return true;

        // $.ajax({
        //     url: 'get_file.php',
        //     type: "POST",
        //     dataType: "binary",
        //     processData:  false,
        //     // contentType: 'application/json',
        //     data: JSON.stringify(categories_data),
        //     // async: false,
        //     success: function (data) {
        //         let filename = 'products.xlsx',
        //             type = 'application/octet-stream',
        //             file = new Blob([data], {type: type});
        //         if (window.navigator.msSaveOrOpenBlob) // IE10+
        //             window.navigator.msSaveOrOpenBlob(file, filename);
        //         else { // Others
        //
        //             let a = document.createElement("a"),
        //                 url = URL.createObjectURL(file);
        //             a.href = url;
        //             a.download = filename;
        //             document.body.appendChild(a);
        //             a.click();
        //
        //             setTimeout(function() {
        //                 document.body.removeChild(a);
        //                 window.URL.revokeObjectURL(url);
        //             }, 0);
        //         }
        //     },
        //     error: function(jqxhr, status, errorMsg) {
        //         $('div#breadcrumbs')
        //             .text("Статус: " + status + " Ошибка: " + errorMsg)
        //             .show();
        //     }
        // });
    }


    /////////////////////////////////////////////////
    function createTree() {
        let tree = tree_config;

        if(! (Array.isArray(category_tree) && category_tree.length)) {
            category_tree = restoreTree();
        }

        tree.core.data = category_tree;

        if(Array.isArray(tree.core.data)) {
            tree.core.data.forEach(function (item, index) {
                item.type = 'default';
                item.url = function (node) {
                    getProducts(node)
                    return '';
                };
            });
        }

        $('#jstree_div').jstree('destroy').jstree(tree)
        // Deselect on click
            .on('click', '.jstree-clicked', function () {
                $('#jstree_div').jstree(true).deselect_node(this);
            })

        // Edit on double click
        // $('#jstree_div').on('dblclick', '.jstree-clicked', function () {
        //     rename_category();
        // });
            .delegate("a","dblclick", function(e) {
                renameCategory();
            })

        // $('#jstree_div').on("changed.jstree", function (e, d) {
        //     console.log(d);
        // });

            .on('changed.jstree', function (e, ref) {
                console.log("on('changed.jstree'", ref);
                $('div#breadcrumbs').text(getCurrentPath(ref)).show();

                getProducts(ref);
            });

        saveTree();
    }

    function deleteAllCategories() {
        categories_data = {}
        category_tree = [];
        category_next_id = 1;
        eraseCookie('category_tree');
        createTree();
        $('#products').html('').show();
    }


    function getCurrentPath(ref, search_string = false, sep = '/') {
        let path = '';
        if(ref && ref.selected && ref.selected.length) {
            if(search_string) {
                path = '"' + ref.instance.get_path(ref.node, '" "', null) + '"';
            } else {
                path = ref.instance.get_path(ref.node, sep, null);
            }
        }

        if(path.length < 3)
            return false;
        return path;
    }


    function addCategory() {
        let ref = $('#jstree_div').jstree(true),
            template = node_template,
            sel = null,
            parent = ref.get_selected();
        if(parent.length) {
            parent = parent[0];
        } else {
            // template.type = "root";
            // template.parent = "#";
            parent = null;
        }

        template.id = category_next_id;
        sel = ref.create_node(parent, template);
        if(sel) {
            category_next_id++;
            ref.edit(sel);
            saveTree();
        }
    }


    function renameCategory() {
        let ref = $('#jstree_div').jstree(true),
            sel = ref.get_selected();
        if(!sel.length) { return false; }
        sel = sel[0];
        ref.edit(sel);
        saveTree();
    }


    function deleteCategory() {
        let ref = $('#jstree_div').jstree(true),
            sel = ref.get_selected();
        if(!sel.length) { return false; }
        ref.delete_node(sel);
        saveTree();
    }

    function restoreTree() {
        return JSON.parse(readCookie("category_tree"));
    }

    function saveTree() {
        // createCookie("category_tree", JSON.stringify(tree));
        let tree = [];
        let t = $("#jstree_div").jstree(true).get_json('#', {'flat': true});
        let cnt = 0;

        console.log(t);
        t.forEach(function(node) {
            cnt++;
            tree.push({
                'id': node['id'],
                'parent': node['parent'],
                'text': node['text'],
            });
        });
        console.log('Nodes: ' + cnt);
        createCookie("category_tree", JSON.stringify(tree));
    }


    function createCookie(name, value, days) {
        let expires;
        if (days) {
            let date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        let cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
        console.log("cookie.length: " + cookie.length);
        document.cookie = cookie;
    }

    function readCookie(name) {
        var nameEQ = encodeURIComponent(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0)
                return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name, "", -1);
    }
</script>


<?// require_once '../counter.php';?>

</body>
</html>