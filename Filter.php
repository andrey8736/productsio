<?php


namespace productsio;


class Filter
{
    protected $data;
    protected  $template;

    public function __construct(&$json_data, $template)
    {
        $this->data = json_decode($json_data, true);
        $this->template = $template;
    }


    private function getCategoryId($category)
    {
        // todo
        return $category;
        // return rand(1,999);
    }


    private function translit($value)
    {
        $converter = [
            'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
            'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
            'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
            'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
            'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
            'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
            'э' => 'e',    'ю' => 'yu',   'я' => 'ya',
        ];

        $value = mb_strtolower($value);
        $value = strtr($value, $converter);
        $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
        $value = mb_ereg_replace('[-]+', '-', $value);
        $value = trim($value, '-');

        return $value;
    }


    public function getSheets()
    {
        $sheets = [];
        switch ($this->template) {
            case 'opencart':
                foreach($this->data as $category=>$items_of_category){
                    // model, vendor, shop, offer
                    foreach ($items_of_category as $item_id=>$item) {

                        // Sheet 1 Products
                        $sheet_num = 1;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['name(ru-ru)'] = $item['model'][$item_id]['name'];
                        $sheets[$sheet_num][$item_id]['categories'] = $this->getCategoryId($category);
                        $sheets[$sheet_num][$item_id]['sku'] = '';
                        $sheets[$sheet_num][$item_id]['upc'] = '';
                        $sheets[$sheet_num][$item_id]['ean'] = '';
                        $sheets[$sheet_num][$item_id]['jan'] = '';
                        $sheets[$sheet_num][$item_id]['isbn'] = '';
                        $sheets[$sheet_num][$item_id]['mpn'] = '';
                        $sheets[$sheet_num][$item_id]['location'] = '';
                        $sheets[$sheet_num][$item_id]['quantity'] = rand(3,17);
                        $sheets[$sheet_num][$item_id]['model'] = '';
                        // manufacturer
                        foreach($item['vendor'] as $vendor){
                            $sheets[$sheet_num][$item_id]['manufacturer'] = $vendor['name'] ?? '';
                            break;
                        }

                        // image_name
                        // if( ! is_dir( 'img' ) ) mkdir( 'img', 0777 );
                        // todo
                        $image_file_name = './tmp/img/' . $this->translit($item['model'][$item_id]['name']) . '_' . $item_id . '.jpeg';
                        file_put_contents($image_file_name, $item['model'][$item_id]['photo']['url']);

                        $sheets[$sheet_num][$item_id]['image_name'] = $image_file_name;
                        $sheets[$sheet_num][$item_id]['shipping'] = 'yes';
                        $sheets[$sheet_num][$item_id]['price'] = $item['model'][$item_id]['price']['avg'] ?? '';
                        $sheets[$sheet_num][$item_id]['points'] = (int) ($item['model'][$item_id]['price']['avg'] ?? '0');
                        $sheets[$sheet_num][$item_id]['date_added'] = '';
                        $sheets[$sheet_num][$item_id]['date_modified'] = '';
                        $sheets[$sheet_num][$item_id]['date_available'] = '';
                        $sheets[$sheet_num][$item_id]['weight'] = '';
                        $sheets[$sheet_num][$item_id]['weight_unit'] = '';
                        $sheets[$sheet_num][$item_id]['length'] = '';
                        $sheets[$sheet_num][$item_id]['width'] = '';
                        $sheets[$sheet_num][$item_id]['height'] = '';
                        $sheets[$sheet_num][$item_id]['length_unit'] = '';
                        $sheets[$sheet_num][$item_id]['status'] = 'true';
                        $sheets[$sheet_num][$item_id]['tax_class_id'] = '';
                        $sheets[$sheet_num][$item_id]['description(ru-ru)'] = $item['model'][$item_id]['name'];
                        $sheets[$sheet_num][$item_id]['meta_title(ru-ru)'] = '';
                        $sheets[$sheet_num][$item_id]['meta_description(ru-ru)'] = '';
                        $sheets[$sheet_num][$item_id]['meta_keywords(ru-ru)'] = '';
                        $sheets[$sheet_num][$item_id]['stock_status_id'] = '7';
                        $sheets[$sheet_num][$item_id]['store_ids'] = '0';
                        $sheets[$sheet_num][$item_id]['layout'] = '';
                        $sheets[$sheet_num][$item_id]['related_ids'] = '';
                        $sheets[$sheet_num][$item_id]['tags(ru-ru)'] = '';
                        $sheets[$sheet_num][$item_id]['sort_order'] = '0';
                        $sheets[$sheet_num][$item_id]['subtract'] = 'true';
                        $sheets[$sheet_num][$item_id]['minimum'] = '1';

                        // Sheet 2 AdditionalImages
                        $sheet_num = 2;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['image'] = $image_file_name;
                        $sheets[$sheet_num][$item_id]['sort_order'] = '0';

                        // Sheet 3 Specials
                        $sheet_num = 3;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['customer_group'] = 'Default';
                        $sheets[$sheet_num][$item_id]['priority'] = '1';
                        $sheets[$sheet_num][$item_id]['price'] = $item['model'][$item_id]['price']['avg'] ?? '';
                        $sheets[$sheet_num][$item_id]['date_start'] = '0000-00-00';
                        $sheets[$sheet_num][$item_id]['date_end'] = '0000-00-00';

                        // Sheet 4 Discounts
                        $sheet_num = 4;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['customer_group'] = 'Default';
                        $sheets[$sheet_num][$item_id]['quantity'] = '0';
                        $sheets[$sheet_num][$item_id]['priority'] = '1';
                        $sheets[$sheet_num][$item_id]['price'] = $item['model'][$item_id]['price']['avg'] ?? '';
                        $sheets[$sheet_num][$item_id]['date_start'] = '0000-00-00';
                        $sheets[$sheet_num][$item_id]['date_end'] = '0000-00-00';

                        // Sheet 5 - Rewards
                        $sheet_num = 5;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['customer_group'] = 'Default';
                        $sheets[$sheet_num][$item_id]['points'] = (int) ($item['model'][$item_id]['price']['avg'] ?? '0');

                        // Sheet 6 ProductOptions
                        $sheet_num = 6;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['option'] = 'Select';
                        $sheets[$sheet_num][$item_id]['default_option_value'] = '';
                        $sheets[$sheet_num][$item_id]['required'] = 'true';

                        // Sheet 7 ProductOptionValues
                        $sheet_num = 7;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['option'] = 'Select';
                        $sheets[$sheet_num][$item_id]['option_value'] = 'Red';
                        $sheets[$sheet_num][$item_id]['quantity'] = '2';
                        $sheets[$sheet_num][$item_id]['subtract'] = 'true';
                        $sheets[$sheet_num][$item_id]['price'] = '"0,00"';
                        $sheets[$sheet_num][$item_id]['price_prefix'] = '+';
                        $sheets[$sheet_num][$item_id]['points'] = '0';
                        $sheets[$sheet_num][$item_id]['points_prefix'] = '+';
                        $sheets[$sheet_num][$item_id]['weight'] = '"0,00"';
                        $sheets[$sheet_num][$item_id]['weight_prefix'] = '+';

                        // Sheet 8 ProductAttributes
                        $sheet_num = 8;
                        foreach($item['model'][$item_id]['specification'] as $group){

                            foreach ($group['features'] as $feature){
                                $sheets[$sheet_num][$item_id]['product_id'] = $item_id;

                                //attribute_group
                                $sheets[$sheet_num][$item_id]['attribute_group'] = $group['name'];

                                $sheets[$sheet_num][$item_id]['attribute'] = $feature['name'];
                                $sheets[$sheet_num][$item_id]['text(ru-ru)'] = $feature['value'];
                            }
                        }

                        //// Sheet 9 ProductFilters
                        // $sheet_num = 9;
                        // $sheets[$sheet_num][$item_id]['product_id'] = $item_id;

                        // Sheet 9 ProductSEOKeywords
                        $sheet_num = 10;
                        $sheets[$sheet_num][$item_id]['product_id'] = $item_id;
                        $sheets[$sheet_num][$item_id]['store_id'] = '0';
                        $sheets[$sheet_num][$item_id]['keyword(ru-ru)'] = $item['model'][$item_id]['name'];
                    }
                }
                break;
        }

        return $sheets;
    }
}