<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once $_SERVER["DOCUMENT_ROOT"] . '/bkp_allinc.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Выгрузка товаров для опенкарт</title>
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://cdn.ingdg.com/get/css/bootstrav v4.0.0/css/bootstrap.min.css" />
</head>
<body>
<? require_once $_SERVER["DOCUMENT_ROOT"].'/bkp_navbar.php';?>

<div class="container">
    <div class="row">
        <div class="col">

            <? require_once '../../public_html/views/jumbotron/OpenCart.php';?>

        </div>
    </div>
</div>


<script src="https://cdn.ingdg.com/get/js/jquery/jquery-3.4.1.min.js"></script>
<script async src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
<script async src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script async src="https://cdn.ingdg.com/get/js/popper.js/popper.min.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138257986-2"></script>
<script async src="https://cdn.ingdg.com/get/css/bootstrav v4.0.0/js/bootstrap.min.js"></script>
<script async src="https://wf.ingdg.com/AuthData"></script>
<script>
    <? require_once '../../public_html/footerJS/opencart.js';?>
</script>

<? require_once '../counter.php';?>

</body>
</html>