<?php
class ControllerExtensionExportImport extends Controller { 
	private $error = array();
	private $ssl = 'SSL';

	public function __construct( $registry ) {

        parent::__construct( $registry );
        $this->ssl = true;


        if (!$this->user->isLogged()){
            if (!isset($_REQUEST['crontoken']) || $_REQUEST['crontoken'] != CRON_TOKEN){
                die('No token or is invalid');
            }
        }

	}


	public function index() {

		$this->load->language('extension/export_import');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('extension/export_import');
		$this->getForm();
	}


	public function upload() {
		$this->load->language('extension/export_import');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('extension/export_import');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validateUploadForm())) {
			if ((isset( $this->request->files['upload'] )) && (is_uploaded_file($this->request->files['upload']['tmp_name']))) {
				$file = $this->request->files['upload']['tmp_name'];
				$incremental = ($this->request->post['incremental']) ? true : false;
				if ($this->model_extension_export_import->upload($file,$this->request->post['incremental'])==true) {
					$this->session->data['success'] = $this->language->get('text_success');
					$this->response->redirect($this->url->link('extension/export_import', 'user_token=' . $this->session->data['user_token'], $this->ssl));
				}
				else {
					$this->session->data['warning'] = $this->language->get('error_upload');
					$href = $this->url->link( 'tool/log', 'user_token='.$this->session->data['user_token'], $this->ssl );
					$this->session->data['warning'] .= "<br />\n".str_replace('%1',$href,$this->language->get( 'text_log_details_3_x' ));
					$this->response->redirect($this->url->link('extension/export_import', 'user_token=' . $this->session->data['user_token'], $this->ssl));
				}
			}
		}

		$this->getForm();
	}

    public function silentUpload($file, $incremental = 1) {

	    //echo $file;



	    //echo $incremental;
	    //die('ggg');
        $this->load->language('extension/export_import');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('extension/export_import');


        //$file = $this->request->files['upload']['tmp_name'];
        //$incremental = ($this->request->post['incremental']) ? true : false;
        if ($this->model_extension_export_import->upload($file, $incremental)==true) {
            return true; //$this->language->get('text_success');

        }
        else {
            return false; //$this->language->get('error_upload');
        }

        die();
        //$this->getForm();
    }


	protected function return_bytes($val)
	{
		$val = trim($val);
	
		switch (strtolower(substr($val, -1)))
		{
			case 'm': $val = (int)substr($val, 0, -1) * 1048576; break;
			case 'k': $val = (int)substr($val, 0, -1) * 1024; break;
			case 'g': $val = (int)substr($val, 0, -1) * 1073741824; break;
			case 'b':
				switch (strtolower(substr($val, -2, 1)))
				{
					case 'm': $val = (int)substr($val, 0, -2) * 1048576; break;
					case 'k': $val = (int)substr($val, 0, -2) * 1024; break;
					case 'g': $val = (int)substr($val, 0, -2) * 1073741824; break;
					default : break;
				} break;
			default: break;
		}
		return $val;
	}


	public function download() {
		$this->load->language( 'extension/export_import' );
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model( 'extension/export_import' );
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateDownloadForm()) {
			$export_type = $this->request->post['export_type'];
			switch ($export_type) {
				case 'c':
				case 'p':
				case 'u':
					$min = null;
					if (isset( $this->request->post['min'] ) && ($this->request->post['min']!='')) {
						$min = $this->request->post['min'];
					}
					$max = null;
					if (isset( $this->request->post['max'] ) && ($this->request->post['max']!='')) {
						$max = $this->request->post['max'];
					}
					if (($min==null) || ($max==null)) {
						$this->model_extension_export_import->download($export_type, null, null, null, null);
					} else if ($this->request->post['range_type'] == 'id') {
						$this->model_extension_export_import->download($export_type, null, null, $min, $max);
					} else {
						$this->model_extension_export_import->download($export_type, $min*($max-1-1), $min, null, null);
					}
					break;
				case 'o':
					$this->model_extension_export_import->download('o', null, null, null, null);
					break;
				case 'a':
					$this->model_extension_export_import->download('a', null, null, null, null);
					break;
				case 'f':
					if ($this->model_extension_export_import->existFilter()) {
						$this->model_extension_export_import->download('f', null, null, null, null);
						break;
					}
					break;
				default:
					break;
			}
			$this->response->redirect( $this->url->link( 'extension/export_import', 'user_token='.$this->request->get['user_token'], $this->ssl) );
		}

		$this->getForm();
	}


	public function settings() {
		$this->load->language('extension/export_import');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('extension/export_import');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validateSettingsForm())) {
			if (!isset($this->request->post['export_import_settings_use_export_cache'])) {
				$this->request->post['export_import_settings_use_export_cache'] = '0';
			}
			if (!isset($this->request->post['export_import_settings_use_import_cache'])) {
				$this->request->post['export_import_settings_use_import_cache'] = '0';
			}
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('export_import', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success_settings');
			$this->response->redirect($this->url->link('extension/export_import', 'user_token=' . $this->session->data['user_token'], $this->ssl));
		}
		$this->getForm();
	}

	public function autoImport(){
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);


        if (isset($_REQUEST['gen_product'])){

            require_once  $_SERVER['DOCUMENT_ROOT'].'/importData/PHPExcel-1.8/Classes/PHPExcel.php';
           // require_once( 'Classes/PHPExcel.php' );
            require_once($_SERVER['DOCUMENT_ROOT'].'/importData/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel5.php');
            // Создаем объект класса PHPExcel
            $xls = PHPExcel_IOFactory::load('/home/igorabes/public_html/importData/products-opencart-template.xlsx');

            $xls_atributes = PHPExcel_IOFactory::load('/home/igorabes/public_html/importData/attributes-opencart-template.xls.xlsx');


            $product_search = '%D0%A8%D1%83%D1%80%D1%83%D0%BF%D0%BE%D0%B2%D0%B5%D1%80%D1%82';

            $addOrnew = 1;

            $loadComments = 1;

            if (isset($_REQUEST['product_search'])){
                $product_search = $_REQUEST['product_search'];
            }

            if (isset($_REQUEST['addOrnew'])){
                $addOrnew = $_REQUEST['addOrnew'];
            }



            $importConfig = ['product_search' => $product_search, 'addOrnew' => $addOrnew, 'loadComments' => $loadComments];

            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/importConfig.txt', serialize($importConfig));

            $productSearch = explode(", ", $product_search);

            foreach ($productSearch as $valueSearch){

                echo $valueSearch;
                $url = 'https://api.ingdg.com/api/productoffers/method/get/'.urlencode($valueSearch).'/';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                //curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                //curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                //curl_setopt($curl, CURLOPT_USERPWD, $client_credentials);
                $data = curl_exec($curl);
                curl_close($curl);


               // print_r($data);

                //die();

                $data = json_decode($data, true);

                //print_r($data);

                //die();

                $i = 2;
                $iter = 2;
                $product_id = 2020;
                $attr_data = [];
                foreach ($data as $k => $v){

                    $product_id= $k;
                    $photoname = 'catalog/dataimport/'.$this->translit_sef($v['model'][$k]['name']).'_'.$product_id.'.jpeg';
                    $photofile = file_get_contents($v['model'][$k]['photo']['url']);

                    $description = '';
                    foreach ($v['offer'] as $koffer1 => $voffer1){

                        $description = '<span style=font-size:24px;>'.$voffer1['name'].'</span>';
                        //$description = '<b>Цена: '.$voffer1['price']['value'].'</b><br>';
                        //$description = '<a target="_blank" href="'.$voffer1['url'].'">Источник</a><br>';

                        //$description.= '<p>';


                    }
                    file_put_contents('/home/igorabes/public_html/image/'.$photoname, $photofile);

                    $product_id+=$i;
// Получаем активный лист 'Products'
                    $xls->setActiveSheetIndex(0);
                    $sheet = $xls->getActiveSheet();

                    if (isset($loadComments) && $loadComments == 1){



                        //$k;

                        $url = 'https://api.ingdg.com/api/productoffers/comments/method/get/'.$k.'/';

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, $url);
                        //curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                        //curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        //curl_setopt($curl, CURLOPT_USERPWD, $client_credentials);
                        $commentData = curl_exec($curl);
                        curl_close($curl);


                        $commentData = json_decode($commentData, true);


                        foreach ($commentData as $commentK => $commentV){

                            $comment_text = $commentV['pros']['text'];
                            $comment_autor = $commentV['author']['name'];
                            $comment_grade = $commentV['grade'];

                            $sql = "INSERT INTO `ocyh_review` (`review_id`, `product_id`, `customer_id`, `author`, `text`, `rating`, `status`, `date_added`, `date_modified`) 
                                VALUES (NULL, '".$product_id."', '0', '".$comment_autor."', '".$comment_text."', '".$comment_grade."', '1', '".date("Y-m-d h:i:s", time())."', '')";

                            $res = $this->db->query( $sql );
                            //print_r($res);

                            //die();
                        }



                        //die();
                    }

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //name(ru-ru)
                    $sheet->setCellValueByColumnAndRow(1,$i, $v['model'][$k]['name']);
                    //categories
                    $sheet->setCellValueByColumnAndRow(2,$i, rand(59,61));
                    //sku
                    $sheet->setCellValueByColumnAndRow(3,$i, '');
                    //upc
                    $sheet->setCellValueByColumnAndRow(4,$i, '');
                    //ean
                    $sheet->setCellValueByColumnAndRow(5,$i, '');
                    //jan
                    $sheet->setCellValueByColumnAndRow(6,$i, '');
                    //isbn
                    $sheet->setCellValueByColumnAndRow(7,$i, '');
                    //mpn
                    $sheet->setCellValueByColumnAndRow(8,$i, '');
                    //location
                    $sheet->setCellValueByColumnAndRow(9,$i, '');
                    //quantity
                    $sheet->setCellValueByColumnAndRow(10,$i, '939');
                    //model
                    $sheet->setCellValueByColumnAndRow(11,$i, 'Product 1');
                    //manufacturer
                    $sheet->setCellValueByColumnAndRow(12,$i, 'HTC');
                    //image_name
                    $sheet->setCellValueByColumnAndRow(13,$i, $photoname);
                    //shipping
                    $sheet->setCellValueByColumnAndRow(14,$i, 'yes');
                    //price
                    $sheet->setCellValueByColumnAndRow(15,$i, '"'.$v['model'][$k]['price']['avg'].'"');
                    //points
                    $sheet->setCellValueByColumnAndRow(16,$i, '200');
                    //date_added
                    $sheet->setCellValueByColumnAndRow(17,$i, '2009-02-03 16:06:50');
                    //date_modified
                    $sheet->setCellValueByColumnAndRow(18,$i, '2011-09-30 01:05:39');
                    //date_available
                    $sheet->setCellValueByColumnAndRow(19,$i, '2009-02-03');
                    //weight
                    $sheet->setCellValueByColumnAndRow(20,$i, '"23.00"');
                    //weight_unit
                    $sheet->setCellValueByColumnAndRow(21,$i, 'g');
                    //length
                    $sheet->setCellValueByColumnAndRow(22,$i, '0');
                    //width
                    $sheet->setCellValueByColumnAndRow(23,$i, '0');
                    //height
                    $sheet->setCellValueByColumnAndRow(24,$i, '0');
                    //length_unit
                    $sheet->setCellValueByColumnAndRow(25,$i, 'cm');
                    //status
                    $sheet->setCellValueByColumnAndRow(26,$i, 'true');
                    //tax_class_id
                    $sheet->setCellValueByColumnAndRow(27,$i, '9');
                    //description(ru-ru)
                    $sheet->setCellValueByColumnAndRow(28,$i, $description);
                    //meta_title(ru-ru)
                    $sheet->setCellValueByColumnAndRow(29,$i, 'HTC Touch HD');
                    //meta_description(ru-ru)
                    $sheet->setCellValueByColumnAndRow(30,$i, '');
                    //meta_keywords(ru-ru)
                    $sheet->setCellValueByColumnAndRow(31,$i, '');
                    //stock_status_id
                    $sheet->setCellValueByColumnAndRow(32,$i, '7');
                    //store_ids
                    $sheet->setCellValueByColumnAndRow(33,$i, '0');
                    //layout
                    $sheet->setCellValueByColumnAndRow(34,$i, '');
                    //related_ids
                    $sheet->setCellValueByColumnAndRow(35,$i, '');
                    //tags(ru-ru)
                    $sheet->setCellValueByColumnAndRow(36,$i, '');
                    //sort_order
                    $sheet->setCellValueByColumnAndRow(37,$i, '0');
                    //subtract
                    $sheet->setCellValueByColumnAndRow(38,$i, 'true');
                    //minimum
                    $sheet->setCellValueByColumnAndRow(39,$i, '1');

// Получаем активный лист 'AdditionalImages'
                    $xls->setActiveSheetIndex(1);
                    $sheet = $xls->getActiveSheet();

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //image
                    $sheet->setCellValueByColumnAndRow(1,$i,'catalog/demo/htc_touch_hd_2.jpg');
                    //sort_order
                    $sheet->setCellValueByColumnAndRow(2,$i,'0');

                    // Получаем активный лист 'Specials'
                    $xls->setActiveSheetIndex(2);
                    $sheet = $xls->getActiveSheet();

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //customer_group
                    $sheet->setCellValueByColumnAndRow(1,$i,'Default');
                    //priority
                    $sheet->setCellValueByColumnAndRow(2,$i,'1');
                    //price
                    $sheet->setCellValueByColumnAndRow(3,$i, '"'.$v['model'][$k]['price']['avg'].'"');
                    //date_start
                    $sheet->setCellValueByColumnAndRow(4,$i,'0000-00-00');
                    //date_end
                    $sheet->setCellValueByColumnAndRow(5,$i,'0000-00-00');

// Получаем активный лист 'Discounts'
                    $xls->setActiveSheetIndex(3);
                    $sheet = $xls->getActiveSheet();

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //customer_group
                    $sheet->setCellValueByColumnAndRow(1,$i,'Default');
                    //quantity
                    $sheet->setCellValueByColumnAndRow(2,$i,'10');
                    //priority
                    $sheet->setCellValueByColumnAndRow(3,$i,'1');
                    //price
                    $sheet->setCellValueByColumnAndRow(4,$i,'"88,00"');
                    //date_start
                    $sheet->setCellValueByColumnAndRow(5,$i,'0000-00-00');
                    //date_end
                    $sheet->setCellValueByColumnAndRow(6,$i,'0000-00-00');

// Получаем активный лист 'Rewards'
                    $xls->setActiveSheetIndex(4);
                    $sheet = $xls->getActiveSheet();

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //customer_group
                    $sheet->setCellValueByColumnAndRow(1,$i,'Default');
                    //points
                    $sheet->setCellValueByColumnAndRow(2,$i,'400');

// Получаем активный лист 'ProductOptions'
                    $xls->setActiveSheetIndex(5);
                    $sheet = $xls->getActiveSheet();

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //option
                    $sheet->setCellValueByColumnAndRow(1,$i,'Select');
                    //default_option_value
                    $sheet->setCellValueByColumnAndRow(2,$i,'');
                    //required
                    $sheet->setCellValueByColumnAndRow(3,$i,'true');

// Получаем активный лист 'ProductOptionValues'
                    $xls->setActiveSheetIndex(6);
                    $sheet = $xls->getActiveSheet();

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //option
                    $sheet->setCellValueByColumnAndRow(1,$i,'Select');
                    //option_value
                    $sheet->setCellValueByColumnAndRow(2,$i,'Red');
                    //quantity
                    $sheet->setCellValueByColumnAndRow(3,$i,'2');
                    //subtract
                    $sheet->setCellValueByColumnAndRow(4,$i,'true');
                    //price
                    $sheet->setCellValueByColumnAndRow(5,$i,'"0,00"');
                    //price_prefix
                    $sheet->setCellValueByColumnAndRow(6,$i,'+');
                    //points
                    $sheet->setCellValueByColumnAndRow(7,$i,'0');
                    //points_prefix
                    $sheet->setCellValueByColumnAndRow(8,$i,'+');
                    //weight
                    $sheet->setCellValueByColumnAndRow(9,$i,'"0,00"');
                    //weight_prefix
                    $sheet->setCellValueByColumnAndRow(10,$i,'+');

// Получаем активный лист 'ProductAttributes'
                    $xls->setActiveSheetIndex(7);
                    $sheet = $xls->getActiveSheet();
                    //echo '<pre>';
                    //print_r($v['model'][$k]['specification']);
                    //echo '</pre>';



                    foreach($v['model'][$k]['specification'] as $k => $v){

                        $attribute_group = $v['name'];


                        foreach ($v['features'] as $k => $v){
                            //product_id
                            $sheet->setCellValueByColumnAndRow(0,$iter,$product_id);

                            if(!isset($attr_data[$attribute_group])){
                                $attr_data[$attribute_group] = [];
                            }
                            $attr_data[$attribute_group][] = $v['name'];
                            //attribute_group
                            $sheet->setCellValueByColumnAndRow(1,$iter,$attribute_group); //'Processor'

                            $attribute = $v['name'];
                            //attribute
                            $sheet->setCellValueByColumnAndRow(2,$iter,$attribute);//'Clockspeed'
                            $attribute_value = $v['value'];
                            //text(ru-ru)
                            $sheet->setCellValueByColumnAndRow(3,$iter, $attribute_value);
                            $iter++;
                        }



                        //die();
                    }



// Получаем активный лист 'ProductFilters'
                    $xls->setActiveSheetIndex(8);
                    $sheet = $xls->getActiveSheet();
                    /*
                            //product_id
                            $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                            //filter_group
                            $sheet->setCellValueByColumnAndRow(1,$i,'');
                            //filter
                            $sheet->setCellValueByColumnAndRow(2,$i,'');
                    */
// Получаем активный лист 'ProductSEOKeywords'
                    $xls->setActiveSheetIndex(9);
                    $sheet = $xls->getActiveSheet();

                    //product_id
                    $sheet->setCellValueByColumnAndRow(0,$i,$product_id);
                    //store_id
                    $sheet->setCellValueByColumnAndRow(1,$i,'0');
                    //keyword(ru-ru)
                    $sheet->setCellValueByColumnAndRow(2,$i, $this->translit_sef($v['model'][$k]['name']).'_'.$product_id);

                    $i++;

                }


                // Выводим содержимое файла
                $objWriter = new PHPExcel_Writer_Excel5($xls);
                $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/gen_products.xls');

                $i = 2;
                $attr_id = 2;
                foreach ($attr_data as $k => $v){
                    $xls_atributes->setActiveSheetIndex(0);
                    $sheet = $xls_atributes->getActiveSheet();
                    $sheet->setCellValueByColumnAndRow(0,$i,$i);
                    $sheet->setCellValueByColumnAndRow(1,$i,0);
                    $sheet->setCellValueByColumnAndRow(2,$i,$k);
                    $sheet->setCellValueByColumnAndRow(3,$i,$k);



                    if (is_array($v)){
                        $xls_atributes->setActiveSheetIndex(1);
                        $sheet_attr = $xls_atributes->getActiveSheet();
                        $it = 2;
                        foreach ($v as $k1 => $v1){

                            $sheet_attr->setCellValueByColumnAndRow(0,$attr_id,$attr_id);
                            $sheet_attr->setCellValueByColumnAndRow(1,$attr_id,$i);
                            $sheet_attr->setCellValueByColumnAndRow(2,$attr_id,0);
                            $sheet_attr->setCellValueByColumnAndRow(3,$attr_id,$v1);
                            $sheet_attr->setCellValueByColumnAndRow(4,$attr_id,$v1);
                            $attr_id++;
                            $it++;

                        }

                    }

                    $i++;

                    // echo 'gg';
                }
                $objWriter_attr = new PHPExcel_Writer_Excel5($xls_atributes);
                $objWriter_attr->save($_SERVER['DOCUMENT_ROOT'].'/gen_attributes.xls');

                $this->silentUpload($_SERVER['DOCUMENT_ROOT'].'/gen_attributes.xls', $addOrnew);
                $this->silentUpload($_SERVER['DOCUMENT_ROOT'].'/gen_products.xls', $addOrnew);

                echo '<a href="/gen_products.xls">Скачать файл импорта продуктов</a><br>';
                echo '<a href="/gen_attributes.xls">Скачать файл атрибутов</a>';

            }




        }

        if (isset($_REQUEST['post_obrabotka'])){

            $sql = "TRUNCATE `igorabes_ocar565`.`ocyh_product_to_category`";
            $this->db->query( $sql );
            $sql = "SELECT * FROM `ocyh_product` WHERE `ocyh_product`.`status` = 1 ORDER BY `ocyh_product`.`price` ASC limit 20";
            $res = $this->db->query( $sql );

            //print_r($res)
            $category_one = [];


            foreach ($res->rows as $k => $v){

                $sql = "INSERT INTO `ocyh_product_to_category` (`product_id`, `category_id`) VALUES ('".$v['product_id']."', '59');";

                $this->db->query( $sql );
                print_r($v['price']); echo "\n";
                //$v['product_id'];
                $category_one[] = $v['product_id'];
                //print_r($v);

            }
            print_r($category_one);
            $sql = "SELECT * FROM `ocyh_product` WHERE `ocyh_product`.`status` = 1 AND product_id NOT IN (".implode(",", $category_one).") ORDER BY `ocyh_product`.`price`  DESC limit 20";
            $res = $this->db->query( $sql );

            //print_r($res);


            $category_two = [];


            foreach ($res->rows as $k => $v){
                $sql = "INSERT INTO `ocyh_product_to_category` (`product_id`, `category_id`) VALUES ('".$v['product_id']."', '60');";

                $this->db->query( $sql );
                print_r($v['price']);  echo "\n";
                $category_two[] = $v['product_id'];
                //print_r($v);

            }

            print_r($category_two);




            $sql ="SELECT ocyh_product.product_id, 

(SELECT COUNT(`ocyh_review`.`review_id` ) FROM `ocyh_review` WHERE `product_id` = ocyh_product.product_id)  as countReviews
FROM `ocyh_product` 


WHERE `ocyh_product`.`status` = 1 AND `ocyh_product`.`product_id` NOT IN (".implode(",", $category_one).", ".implode(",", $category_two).") ORDER BY countReviews DESC limit 20";

            $res = $this->db->query( $sql );

            //print_r($res);


            $category_three = [];


            foreach ($res->rows as $k => $v){

                $sql = "INSERT INTO `ocyh_product_to_category` (`product_id`, `category_id`) VALUES ('".$v['product_id']."', '61');";

                $this->db->query( $sql );
                print_r($v['price']);  echo "\n";
                $category_three[] = $v['product_id'];
                //print_r($v);

            }

            print_r($category_three);

            die('post_obrabotka');
        }

	    die('gg');
    }


	protected function getForm() {
		$data = array();
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['exist_filter'] = $this->model_extension_export_import->existFilter();

		$data['text_export_type_category'] = ($data['exist_filter']) ? $this->language->get('text_export_type_category') : $this->language->get('text_export_type_category_old');
		$data['text_export_type_product'] = ($data['exist_filter']) ? $this->language->get('text_export_type_product') : $this->language->get('text_export_type_product_old');
		$data['text_export_type_poa'] = $this->language->get('text_export_type_poa');
		$data['text_export_type_option'] = $this->language->get('text_export_type_option');
		$data['text_export_type_attribute'] = $this->language->get('text_export_type_attribute');
		$data['text_export_type_filter'] = $this->language->get('text_export_type_filter');
		$data['text_export_type_customer'] = $this->language->get('text_export_type_customer');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_loading_notifications'] = $this->language->get( 'text_loading_notifications' );
		$data['text_retry'] = $this->language->get('text_retry');
		$data['text_used_category_ids'] = $this->language->get('text_used_category_ids');
		$data['text_used_product_ids'] = $this->language->get('text_used_product_ids');

		$data['entry_export'] = $this->language->get( 'entry_export' );
		$data['entry_import'] = $this->language->get( 'entry_import' );
		$data['entry_export_type'] = $this->language->get( 'entry_export_type' );
		$data['entry_range_type'] = $this->language->get( 'entry_range_type' );
		$data['entry_category_filter'] = $this->language->get( 'entry_category_filter' );
		$data['entry_category'] = $this->language->get( 'entry_category' );
		$data['entry_start_id'] = $this->language->get( 'entry_start_id' );
		$data['entry_start_index'] = $this->language->get( 'entry_start_index' );
		$data['entry_end_id'] = $this->language->get( 'entry_end_id' );
		$data['entry_end_index'] = $this->language->get( 'entry_end_index' );
		$data['entry_incremental'] = $this->language->get( 'entry_incremental' );
		$data['entry_upload'] = $this->language->get( 'entry_upload' );
		$data['entry_settings_use_option_id'] = $this->language->get( 'entry_settings_use_option_id' );
		$data['entry_settings_use_option_value_id'] = $this->language->get( 'entry_settings_use_option_value_id' );
		$data['entry_settings_use_attribute_group_id'] = $this->language->get( 'entry_settings_use_attribute_group_id' );
		$data['entry_settings_use_attribute_id'] = $this->language->get( 'entry_settings_use_attribute_id' );
		$data['entry_settings_use_filter_group_id'] = $this->language->get( 'entry_settings_use_filter_group_id' );
		$data['entry_settings_use_filter_id'] = $this->language->get( 'entry_settings_use_filter_id' );
		$data['entry_settings_use_export_cache'] = $this->language->get( 'entry_settings_use_export_cache' );
		$data['entry_settings_use_import_cache'] = $this->language->get( 'entry_settings_use_import_cache' );

		$data['tab_export'] = $this->language->get( 'tab_export' );
		$data['tab_import'] = $this->language->get( 'tab_import' );
		$data['tab_settings'] = $this->language->get( 'tab_settings' );

		$data['button_export'] = $this->language->get( 'button_export' );
		$data['button_import'] = $this->language->get( 'button_import' );
		$data['button_settings'] = $this->language->get( 'button_settings' );
		$data['button_export_id'] = $this->language->get( 'button_export_id' );
		$data['button_export_page'] = $this->language->get( 'button_export_page' );

		$data['help_range_type'] = $this->language->get( 'help_range_type' );
		$data['help_category_filter'] = $this->language->get( 'help_category_filter' );
		$data['help_incremental_yes'] = $this->language->get( 'help_incremental_yes' );
		$data['help_incremental_no'] = $this->language->get( 'help_incremental_no' );
		$data['help_import'] = ($data['exist_filter']) ? $this->language->get( 'help_import' ) : $this->language->get( 'help_import_old' );
		$data['help_format'] = $this->language->get( 'help_format' );

		$data['error_select_file'] = $this->language->get('error_select_file');
		$data['error_post_max_size'] = str_replace( '%1', ini_get('post_max_size'), $this->language->get('error_post_max_size') );
		$data['error_upload_max_filesize'] = str_replace( '%1', ini_get('upload_max_filesize'), $this->language->get('error_upload_max_filesize') );
		$data['error_id_no_data'] = $this->language->get('error_id_no_data');
		$data['error_page_no_data'] = $this->language->get('error_page_no_data');
		$data['error_param_not_number'] = $this->language->get('error_param_not_number');
		$data['error_notifications'] = $this->language->get('error_notifications');
		$data['error_no_news'] = $this->language->get('error_no_news');
		$data['error_batch_number'] = $this->language->get('error_batch_number');
		$data['error_min_item_id'] = $this->language->get('error_min_item_id');

		if (!empty($this->session->data['export_import_error']['errstr'])) {
			$this->error['warning'] = $this->session->data['export_import_error']['errstr'];
		} else if (isset($this->session->data['warning'])) {
			$this->error['warning'] = $this->session->data['warning'];
		}

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
			if (!empty($this->session->data['export_import_nochange'])) {
				$data['error_warning'] .= "<br />\n".$this->language->get( 'text_nochange' );
			}
		} else {
			$data['error_warning'] = '';
		}

		unset($this->session->data['warning']);
		unset($this->session->data['export_import_error']);
		unset($this->session->data['export_import_nochange']);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], $this->ssl)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/export_import', 'user_token=' . $this->session->data['user_token'], $this->ssl)
		);

		$data['back'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], $this->ssl);
		$data['button_back'] = $this->language->get( 'button_back' );
		$data['import'] = $this->url->link('extension/export_import/upload', 'user_token=' . $this->session->data['user_token'], $this->ssl);
		$data['export'] = $this->url->link('extension/export_import/download', 'user_token=' . $this->session->data['user_token'], $this->ssl);
		$data['settings'] = $this->url->link('extension/export_import/settings', 'user_token=' . $this->session->data['user_token'], $this->ssl);
		$data['post_max_size'] = $this->return_bytes( ini_get('post_max_size') );
		$data['upload_max_filesize'] = $this->return_bytes( ini_get('upload_max_filesize') );

		if (isset($this->request->post['export_type'])) {
			$data['export_type'] = $this->request->post['export_type'];
		} else {
			$data['export_type'] = 'p';
		}

		if (isset($this->request->post['range_type'])) {
			$data['range_type'] = $this->request->post['range_type'];
		} else {
			$data['range_type'] = 'id';
		}

		if (isset($this->request->post['min'])) {
			$data['min'] = $this->request->post['min'];
		} else {
			$data['min'] = '';
		}

		if (isset($this->request->post['max'])) {
			$data['max'] = $this->request->post['max'];
		} else {
			$data['max'] = '';
		}

		if (isset($this->request->post['incremental'])) {
			$data['incremental'] = $this->request->post['incremental'];
		} else {
			$data['incremental'] = '1';
		}

		if (isset($this->request->post['export_import_settings_use_option_id'])) {
			$data['settings_use_option_id'] = $this->request->post['export_import_settings_use_option_id'];
		} else if ($this->config->get( 'export_import_settings_use_option_id' )) {
			$data['settings_use_option_id'] = '1';
		} else {
			$data['settings_use_option_id'] = '0';
		}

		if (isset($this->request->post['export_import_settings_use_option_value_id'])) {
			$data['settings_use_option_value_id'] = $this->request->post['export_import_settings_use_option_value_id'];
		} else if ($this->config->get( 'export_import_settings_use_option_value_id' )) {
			$data['settings_use_option_value_id'] = '1';
		} else {
			$data['settings_use_option_value_id'] = '0';
		}

		if (isset($this->request->post['export_import_settings_use_attribute_group_id'])) {
			$data['settings_use_attribute_group_id'] = $this->request->post['export_import_settings_use_attribute_group_id'];
		} else if ($this->config->get( 'export_import_settings_use_attribute_group_id' )) {
			$data['settings_use_attribute_group_id'] = '1';
		} else {
			$data['settings_use_attribute_group_id'] = '0';
		}

		if (isset($this->request->post['export_import_settings_use_attribute_id'])) {
			$data['settings_use_attribute_id'] = $this->request->post['export_import_settings_use_attribute_id'];
		} else if ($this->config->get( 'export_import_settings_use_attribute_id' )) {
			$data['settings_use_attribute_id'] = '1';
		} else {
			$data['settings_use_attribute_id'] = '0';
		}

		if (isset($this->request->post['export_import_settings_use_filter_group_id'])) {
			$data['settings_use_filter_group_id'] = $this->request->post['export_import_settings_use_filter_group_id'];
		} else if ($this->config->get( 'export_import_settings_use_filter_group_id' )) {
			$data['settings_use_filter_group_id'] = '1';
		} else {
			$data['settings_use_filter_group_id'] = '0';
		}

		if (isset($this->request->post['export_import_settings_use_filter_id'])) {
			$data['settings_use_filter_id'] = $this->request->post['export_import_settings_use_filter_id'];
		} else if ($this->config->get( 'export_import_settings_use_filter_id' )) {
			$data['settings_use_filter_id'] = '1';
		} else {
			$data['settings_use_filter_id'] = '0';
		}

		if (isset($this->request->post['export_import_settings_use_export_cache'])) {
			$data['settings_use_export_cache'] = $this->request->post['export_import_settings_use_export_cache'];
		} else if ($this->config->get( 'export_import_settings_use_export_cache' )) {
			$data['settings_use_export_cache'] = '1';
		} else {
			$data['settings_use_export_cache'] = '0';
		}

		if (isset($this->request->post['export_import_settings_use_import_cache'])) {
			$data['settings_use_import_cache'] = $this->request->post['export_import_settings_use_import_cache'];
		} else if ($this->config->get( 'export_import_settings_use_import_cache' )) {
			$data['settings_use_import_cache'] = '1';
		} else {
			$data['settings_use_import_cache'] = '0';
		}

		$data['categories'] = array();

		$min_product_id = $this->model_extension_export_import->getMinProductId();
		$max_product_id = $this->model_extension_export_import->getMaxProductId();
		$count_product = $this->model_extension_export_import->getCountProduct();
		$min_category_id = $this->model_extension_export_import->getMinCategoryId();
		$max_category_id = $this->model_extension_export_import->getMaxCategoryId();
		$count_category = $this->model_extension_export_import->getCountCategory();
		$min_customer_id = $this->model_extension_export_import->getMinCustomerId();
		$max_customer_id = $this->model_extension_export_import->getMaxCustomerId();
		$count_customer = $this->model_extension_export_import->getCountCustomer();
		
		$data['text_used_category_ids'] = str_replace('%1',$min_category_id,$data['text_used_category_ids']);
		$data['text_used_category_ids'] = str_replace('%2',$max_category_id,$data['text_used_category_ids']);
		$data['text_used_product_ids'] = str_replace('%1',$min_product_id,$data['text_used_product_ids']);
		$data['text_used_product_ids'] = str_replace('%2',$max_product_id,$data['text_used_product_ids']);

		$data['min_product_id'] = $min_product_id;
		$data['max_product_id'] = $max_product_id;
		$data['count_product'] = $count_product;
		$data['min_category_id'] = $min_category_id;
		$data['max_category_id'] = $max_category_id;
		$data['count_category'] = $count_category;
		$data['min_customer_id'] = $min_customer_id;
		$data['max_customer_id'] = $max_customer_id;
		$data['count_customer'] = $count_customer;

		$data['user_token'] = $this->session->data['user_token'];

		$this->document->addStyle('view/stylesheet/export_import.css');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['CRON_TOKEN'] = CRON_TOKEN;

		if (file_exists($_SERVER['DOCUMENT_ROOT'].'/importConfig.txt')){
		    $importConfig = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/importConfig.txt');
		    $importConfig = unserialize($importConfig);
		    $data['product_search'] = $importConfig['product_search'];
            $data['addOrnew'] = $importConfig['addOrnew'];
        } else {
            $importConfig = ['product_search' => '', 'addOrnew' => '0', 'loadComments' => '0'];

            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/importConfig.txt', serialize($importConfig));
        }



		$this->response->setOutput($this->load->view( 'extension/export_import', $data));
	}


	protected function validateDownloadForm() {
		if (!$this->user->hasPermission('access', 'extension/export_import')) {
			$this->error['warning'] = $this->language->get('error_permission');
			return false;
		}

		if (!$this->config->get( 'export_import_settings_use_option_id' )) {
			$option_names = $this->model_extension_export_import->getOptionNameCounts();
			foreach ($option_names as $option_name) {
				if ($option_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $option_name['name'], $this->language->get( 'error_option_name' ) );
					return false;
				}
			}
		}

		if (!$this->config->get( 'export_import_settings_use_option_value_id' )) {
			$option_value_names = $this->model_extension_export_import->getOptionValueNameCounts();
			foreach ($option_value_names as $option_value_name) {
				if ($option_value_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $option_value_name['name'], $this->language->get( 'error_option_value_name' ) );
					return false;
				}
			}
		}

		if (!$this->config->get( 'export_import_settings_use_attribute_group_id' )) {
			$attribute_group_names = $this->model_extension_export_import->getAttributeGroupNameCounts();
			foreach ($attribute_group_names as $attribute_group_name) {
				if ($attribute_group_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $attribute_group_name['name'], $this->language->get( 'error_attribute_group_name' ) );
					return false;
				}
			}
		}

		if (!$this->config->get( 'export_import_settings_use_attribute_id' )) {
			$attribute_names = $this->model_extension_export_import->getAttributeNameCounts();
			foreach ($attribute_names as $attribute_name) {
				if ($attribute_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $attribute_name['name'], $this->language->get( 'error_attribute_name' ) );
					return false;
				}
			}
		}

		if (!$this->config->get( 'export_import_settings_use_filter_group_id' )) {
			$filter_group_names = $this->model_extension_export_import->getFilterGroupNameCounts();
			foreach ($filter_group_names as $filter_group_name) {
				if ($filter_group_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $filter_group_name['name'], $this->language->get( 'error_filter_group_name' ) );
					return false;
				}
			}
		}

		if (!$this->config->get( 'export_import_settings_use_filter_id' )) {
			$filter_names = $this->model_extension_export_import->getFilterNameCounts();
			foreach ($filter_names as $filter_name) {
				if ($filter_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $filter_name['name'], $this->language->get( 'error_filter_name' ) );
					return false;
				}
			}
		}

		return true;
	}


	protected function validateUploadForm() {
		if (!$this->user->hasPermission('modify', 'extension/export_import')) {
			$this->error['warning'] = $this->language->get('error_permission');
		} else if (!isset( $this->request->post['incremental'] )) {
			$this->error['warning'] = $this->language->get( 'error_incremental' );
		} else if ($this->request->post['incremental'] != '0') {
			if ($this->request->post['incremental'] != '1') {
				$this->error['warning'] = $this->language->get( 'error_incremental' );
			}
		}

		if (!isset($this->request->files['upload']['name'])) {
			if (isset($this->error['warning'])) {
				$this->error['warning'] .= "<br /\n" . $this->language->get( 'error_upload_name' );
			} else {
				$this->error['warning'] = $this->language->get( 'error_upload_name' );
			}
		} else {
			$ext = strtolower(pathinfo($this->request->files['upload']['name'], PATHINFO_EXTENSION));
			if (($ext != 'xls') && ($ext != 'xlsx') && ($ext != 'ods')) {
				if (isset($this->error['warning'])) {
					$this->error['warning'] .= "<br /\n" . $this->language->get( 'error_upload_ext' );
				} else {
					$this->error['warning'] = $this->language->get( 'error_upload_ext' );
				}
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}


	protected function validateSettingsForm() {
		if (!$this->user->hasPermission('access', 'extension/export_import')) {
			$this->error['warning'] = $this->language->get('error_permission');
			return false;
		}

		if (empty($this->request->post['export_import_settings_use_option_id'])) {
			$option_names = $this->model_extension_export_import->getOptionNameCounts();
			foreach ($option_names as $option_name) {
				if ($option_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $option_name['name'], $this->language->get( 'error_option_name' ) );
					return false;
				}
			}
		}

		if (empty($this->request->post['export_import_settings_use_option_value_id'])) {
			$option_value_names = $this->model_extension_export_import->getOptionValueNameCounts();
			foreach ($option_value_names as $option_value_name) {
				if ($option_value_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $option_value_name['name'], $this->language->get( 'error_option_value_name' ) );
					return false;
				}
			}
		}

		if (empty($this->request->post['export_import_settings_use_attribute_group_id'])) {
			$attribute_group_names = $this->model_extension_export_import->getAttributeGroupNameCounts();
			foreach ($attribute_group_names as $attribute_group_name) {
				if ($attribute_group_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $attribute_group_name['name'], $this->language->get( 'error_attribute_group_name' ) );
					return false;
				}
			}
		}

		if (empty($this->request->post['export_import_settings_use_attribute_id'])) {
			$attribute_names = $this->model_extension_export_import->getAttributeNameCounts();
			foreach ($attribute_names as $attribute_name) {
				if ($attribute_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $attribute_name['name'], $this->language->get( 'error_attribute_name' ) );
					return false;
				}
			}
		}

		if (empty($this->request->post['export_import_settings_use_filter_group_id'])) {
			$filter_group_names = $this->model_extension_export_import->getFilterGroupNameCounts();
			foreach ($filter_group_names as $filter_group_name) {
				if ($filter_group_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $filter_group_name['name'], $this->language->get( 'error_filter_group_name' ) );
					return false;
				}
			}
		}

		if (empty($this->request->post['export_import_settings_use_filter_id'])) {
			$filter_names = $this->model_extension_export_import->getFilterNameCounts();
			foreach ($filter_names as $filter_name) {
				if ($filter_name['count'] > 1) {
					$this->error['warning'] = str_replace( '%1', $filter_name['name'], $this->language->get( 'error_filter_name' ) );
					return false;
				}
			}
		}

		return true;
	}


	public function getNotifications() {
		sleep(1); // give the data some "feel" that its not in our system
		$this->load->model('extension/export_import');
		$this->load->language( 'extension/export_import' );
		$response = $this->model_extension_export_import->getNotifications();
		$json = array();
		if ($response===false) {
			$json['message'] = '';
			$json['error'] = $this->language->get( 'error_notifications' );
		} else {
			$json['message'] = $response;
			$json['error'] = '';
		}
		$this->response->setOutput(json_encode($json));
	}


	public function getCountProduct() {
		$this->load->model('extension/export_import');
		$count = $this->model_extension_export_import->getCountProduct();
		$json = array( 'count'=>$count );
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    function translit_sef($value)
    {
        $converter = array(
            'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
            'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
            'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
            'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
            'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
            'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
            'э' => 'e',    'ю' => 'yu',   'я' => 'ya',
        );

        $value = mb_strtolower($value);
        $value = strtr($value, $converter);
        $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
        $value = mb_ereg_replace('[-]+', '-', $value);
        $value = trim($value, '-');

        return $value;
    }
}
?>