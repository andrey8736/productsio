<?php
defined('_EXEC') or exit();
?>
<nav class="navbar navbar-expand-lg  navbar-dark bg-dark" id="top-a">
    <a class="navbar-brand" rel="nofollow" href="/">
        DATA HUNTER
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="/export-to-opencart/" class="nav-link" title="AI">Выгрузка товаров Opencart</a>
            </li>
        </ul>
        <ul id="acc_list" class="navbar-nav">
        </ul>
    </div>
</nav>
