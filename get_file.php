<?php

define('_EXEC', 'get_file');


include('./config.php');


use productsio\Filter;
use productsio\XlsxDoc;


if (! array_key_exists('categories_data', $_POST))
    exit();


$template = $_POST['template'] ?? '';
$data = new Filter($_POST['categories_data'], $template);
unset($_POST['categories_data']);
$xlsx_products = new XlsxDoc('./templates/opencart/products.xlsx', './tmp');

foreach($data->getSheets() as $sheet_num=>$sheet_data){
    $xlsx_products->addRows($sheet_num, $sheet_data);
}

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="products.xlsx"');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');

$xlsx_products->close();

echo file_get_contents($xlsx_products->getFileName());

exit();


