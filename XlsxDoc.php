<?php


namespace productsio;


use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SimpleXMLElement;
use ZipArchive;


class XlsxDoc
{
    private $template_dir = null;
    private $file_name = null;
    private $zip = null;
    private $alphabet = null;
    private $use_shared_strings = false;
    private $shared_strings = [];
    private $shared_strings_xml = null;
    private $shared_strings_saved_items = 0;
    private $shared_strings_uniqueCount = 0;
    private $shared_strings_count = 0;

    public function __construct($template_dir, $dst_dir=null) {
        $this->alphabet = range('A', 'Z');

        if ($dst_dir == null)
            $dst_dir = '.';

        $this->file_name = $dst_dir . DIRECTORY_SEPARATOR . md5(microtime() . uniqid() . rand(0, 9999)) . '.xlsx';
        $this->file_name = $this->normalizePath($this->file_name);

        $this->template_dir = $this->normalizePath($template_dir);

        $shared_strings_path = $this->template_dir
            . DIRECTORY_SEPARATOR .  'xl' . DIRECTORY_SEPARATOR . 'sharedStrings.xml';
        $this->shared_strings_xml = simplexml_load_file($shared_strings_path);
        $this->shared_strings_uniqueCount = $this->shared_strings_xml[0]['uniqueCount'];
        $this->shared_strings_count = $this->shared_strings_xml[0]['count'];
        foreach ($this->shared_strings_xml->children() as $item) {
            $this->shared_strings[] = (string)$item->t;
            $this->shared_strings_saved_items += 1;
        }

        $this->zip = new ZipArchive();
        if (! $this->zip->open($this->file_name, ZIPARCHIVE::CREATE)) {
            $this->file_name = null;
            return;
        }

        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->template_dir),
            RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file) {
            $file = $this->normalizePath($file);

            // if ($file == $template_dir . '.'
            //     || $file == $template_dir . '..'
            //     || empty($file)
            //     || $file == $template_dir . DIRECTORY_SEPARATOR)
            // {
            //     continue;
            // }

            // Ignore "." and ".." folders
            if (in_array(substr($file, strrpos($file, DIRECTORY_SEPARATOR) + 1), array('.', '..'))) {
                continue;
            }

            if (is_dir($file) === true) {
                $d = str_replace($this->template_dir . DIRECTORY_SEPARATOR, '', $file);
                if (empty($d)) {
                    continue;
                }
                $this->zip->addEmptyDir($d);
            } elseif (is_file($file) === true) {
                $this->zip->addFromString(str_replace($this->template_dir . DIRECTORY_SEPARATOR, '', $file),
                    file_get_contents($file));
            }
        }

    }


    public function close()
    {
        if ($this->zip)
            $this->zip->close();
    }


    public function getFileName()
    {
        return $this->file_name;
    }


    public function num_to_s (int $num)  // 0 -> A
    {
        $base = sizeof($this->alphabet);
        $s = 'A';
        if ($num < $base) {
            $s = $this->alphabet[$num];
        } elseif ($num < $base**2) {
            $s = $this->alphabet[(int)($num / $base) -1];
            $s .= $this->num_to_s($num % $base);
        }
            return $s;
    }

    private function normalizePath($file)
    {
        $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
        $file = str_replace('/', DIRECTORY_SEPARATOR, $file);

        return $file;
    }


    private function getSheetTemplatePath(int $sheet_num, bool $full=true)
    {
        $path = 'xl' . DIRECTORY_SEPARATOR . 'worksheets' . DIRECTORY_SEPARATOR . 'sheet' . $sheet_num . '.xml';
        if ($full) {
            $path = $this->template_dir . DIRECTORY_SEPARATOR . $path;
        }
        return $path;
    }


    private function saveSharedStrings()
    {
        if ($this->use_shared_strings) {
            // $this->shared_strings_xml[0]['uniqueCount'] = $this->shared_strings_uniqueCount;
            // $this->shared_strings_xml[0]['count'] = $this->shared_strings_count;
            while ($this->shared_strings_saved_items < count($this->shared_strings)) {
                $si = $this->shared_strings_xml[0]
                    ->addChild('si')
                    ->addChild('t', $this->shared_strings[$this->shared_strings_saved_items]);
                $si->addAttribute('xml:space', 'preserve');
                $this->shared_strings_saved_items += 1;
            }
            $this->zip->addFromString('xl/sharedStrings.xml', $this->shared_strings_xml->asXML());
        }
    }


    private function saveSheet(int $sheet_num, $xml_string)
    {
        $this->zip->addFromString($this->getSheetTemplatePath($sheet_num, false), $xml_string);
        $this->saveSharedStrings();
    }


    static function getDataFromXlsx($path, $sheet_num = 1)
    {
        $shared_strings_path = "zip://" . $path . '#xl/sharedStrings.xml';
        $shared_strings_arr = [];
        $shared_strings_xml = simplexml_load_file($shared_strings_path);
        foreach ($shared_strings_xml->children() as $item) {
            $shared_strings_arr[] = (string)$item->t;
        }

        $sheet_path = "zip://" . $path . '#xl/worksheets/sheet' . $sheet_num . '.xml';
        $sheet_xml = simplexml_load_file($sheet_path);
        $sheet_data = [];
        $row_cnt = 0;
        foreach ($sheet_xml->sheetData->row as $row) {
            $sheet_data[$row_cnt] = [];
            $cell_cnt = 0;
            foreach ($row as $cell) {
                $attr = $cell->attributes();
                $value = isset($cell->v) ? (string)$cell->v : false;
                $sheet_data[$row_cnt][$cell_cnt] = isset($attr['t']) ? $shared_strings_arr[$value] : $value;
                $cell_cnt += 1;
            }
            $row_cnt += 1;
        }

        return $sheet_data;

        // $ns = $xml->getDocNamespaces();
        // $xml->registerXPathNamespace('w', $ns['w']);

        // return $sheet_xml->sheetData[0]->count();
        // foreach ($xml->xpath('/w:document/w:body/w:tbl') as $elem) {
        // }
    }

    // public function addRows_old(int $sheet_num, array $rows)
    // {
    //     $xml_sheet = new SimpleXMLElement(file_get_contents($this->getSheetTemplatePath($sheet_num)));
    //     $row_num = $xml_sheet->sheetData->count();
    //
    //     foreach ($rows as $row) {
    //         $row_num += 1;
    //         $xml_row = $xml_sheet->sheetData->addChild('row');
    //
    //         foreach ($xml_sheet->sheetData->row[0]->attributes() as $attr => $val) {
    //             $xml_row->addAttribute($attr, $val);
    //         }
    //         $xml_row['r'] =  $row_num;
    //
    //         $i = 0;
    //         $cell_idx = 'A1';
    //         foreach ($row as $cell_data) {
    //             $cell_idx = $this->num_to_s($i) . ($row_num);
    //             $xml_cell = $xml_row->addChild('c');
    //             $xml_cell->addAttribute('r', $cell_idx);
    //             $xml_cell->addAttribute('s', '0');
    //             if (is_numeric($cell_data)) {
    //                 $xml_cell->addAttribute('t', 'n');
    //                 $xml_cell->addChild('v', $cell_data);
    //             } else {
    //                 $xml_cell->addAttribute('t', 'inlineStr');
    //                 $xml_cell->addChild('is')->addChild('t', $cell_data);
    //             }
    //
    //             $i += 1;
    //         }
    //     }
    //     $xml_sheet->dimension['ref'] = 'A1:' . $cell_idx;
    //
    //     $this->saveSheet($sheet_num, $xml_sheet->asXML());
    // }


    private function getSharedStringsIndex($string_val)
    {
        $index = array_search($string_val, $this->shared_strings);
        if($index == '') {
            $this->shared_strings_uniqueCount += 1;
            $this->shared_strings[] = $string_val;
            $index = array_search($string_val, $this->shared_strings);
        }
        $this->shared_strings_count += 1;
        return $index;
    }


    public function addRows(int $sheet_num, array $rows, $use_shared_strings=true)
    {
        $this->use_shared_strings = $use_shared_strings;
        $xml_sheet = new SimpleXMLElement(file_get_contents($this->getSheetTemplatePath($sheet_num)));
        $row_num = $xml_sheet->sheetData->count();

        foreach ($rows as $row) {
            $row_num += 1;
            $xml_row = $xml_sheet->sheetData->addChild('row');

            // foreach ($xml_sheet->sheetData->row[0]->attributes() as $attr => $val) {
            //     $xml_row->addAttribute($attr, $val);
            // }
            $xml_row->addAttribute('r', $row_num);
            $xml_row->addAttribute('s', '0');
            // $xml_row->addAttribute('customFormat', 'true');
            $xml_row->addAttribute('ht', '15');
            $xml_row->addAttribute('hidden', 'false');
            // $xml_row->addAttribute('customHeight', 'true');
            // $xml_row->addAttribute('outlineLevel', '1');
            $xml_row->addAttribute('collapsed', 'false');
            // $xml_row['r'] =  $row_num;

            $i = 0;
            $cell_idx = 'A1';
            foreach ($row as $cell_data) {
                $cell_idx = $this->num_to_s($i) . ($row_num);
                if ($cell_data){
                    $xml_cell = $xml_row->addChild('c');
                    $xml_cell->addAttribute('r', $cell_idx);
                    $xml_cell->addAttribute('s', '0');
                    if (is_numeric($cell_data)) {
                        $xml_cell->addAttribute('t', 'n');
                        $xml_cell->addChild('v', $cell_data);
                    } else {
                        if($use_shared_strings) {
                            $xml_cell->addAttribute('t', 's');
                            $xml_cell->addChild('v', $this->getSharedStringsIndex($cell_data));
                        } else {
                            $xml_cell->addAttribute('t', 'inlineStr');
                            $xml_cell->addChild('is')->addChild('t', $cell_data);
                        }
                    }
                }
                $i += 1;
            }
        }
        $xml_sheet->dimension['ref'] = 'A1:' . $cell_idx;

        $this->saveSheet($sheet_num, $xml_sheet->asXML());
    }
}