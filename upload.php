<?php

define('_EXEC', 'upload');

include('./config.php');


use productsio\XlsxDoc;

// if (! array_key_exists('categories_data', $_POST))
//     exit();

$data = array();

$response = [];


if( ! is_dir( UPLOAD_DIR ) ) mkdir( UPLOAD_DIR, 0777 );

$categories = [];

// $response['error'] = 'Ошибка загрузки файлов';

foreach($_FILES as $file){
    $sheet_data = XlsxDoc::getDataFromXlsx($file['tmp_name'], 1);

    $tree = [];
    $max_category_id = 0;
    $cnt = 0;
    foreach ($sheet_data as $row) {
        if ($cnt) {
            if ($max_category_id < $row[0])
                $max_category_id = $row[0];

            $tree[] = [
                'id' => $row[0],
                'parent' => $row[1] ? $row[1] : '#',
                // 'name' => 'N_' . $row[2],
                'text' => $row[2],
                ];
        }
        $cnt += 1;
    }

    unset($sheet_data);

    $response['tree'] = $tree;
    $response['category_next_id'] = $max_category_id + 1;

    $response['tmp_file'] = $file['tmp_name'];

    // todo del
    if( move_uploaded_file( $file['tmp_name'], UPLOAD_DIR . basename($file['name']) ) ){
        $response['file'] = realpath( UPLOAD_DIR . $file['name'] );
    }
}

echo json_encode( $response );

exit();
